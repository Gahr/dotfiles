return require('packer').startup(function()
    use 'wbthomason/packer.nvim' -- Package manager
    use 'neovim/nvim-lspconfig' -- Configurations for built-in LSP client
    -- use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
    -- use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp
    -- use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp
    use 'L3MON4D3/LuaSnip' -- Snippets plugin
    use {
        'nvim-treesitter/nvim-treesitter', -- Treesitter for syntax highlighting
        run = ':TSUpdate'
    }
    use {
        'nvim-telescope/telescope.nvim', -- Fuzzy finder
        requires = { {'nvim-lua/plenary.nvim'} }
    }
    use 'EdenEast/nightfox.nvim' -- Colorscheme
    use { 'echasnovski/mini.nvim', branch = 'stable' }
    use 'kyazdani42/nvim-web-devicons'
end)
