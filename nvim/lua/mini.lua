require('mini.bufremove').setup() -- Buffer functions

require('mini.completion').setup() -- Autocompletion

require('mini.cursorword').setup()

require('mini.fuzzy').setup()

require('mini.jump').setup()

require('mini.pairs').setup()

require('mini.starter').setup()

require('mini.statusline').setup()

require('mini.tabline').setup()

require('mini.trailspace').setup()
