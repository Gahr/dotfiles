local map = vim.api.nvim_set_keymap

-- Set leader key.
vim.g.mapleader = ','

options = { noremap = true }
-- Disable arrow keys in normal mode
map('n', '<left>', '<nop>', options)
map('n', '<right>', '<nop>', options)
map('n', '<up>', '<nop>', options)
map('n', '<down>', '<nop>', options)

-- Buffer movement
map('n', '<Tab>', ':bn<CR>', options)
map('n', '<S-Tab>', ':bp<CR>', options)
map('n', '<d><Tab>', ':bd<CR>', options)

-- Remap esc in terminal mode
map('t', '<Esc>', '<C-\\><C-n>', options)

-- Trim trailing whitespaces.
map('n', '<silent><leader>t', ':%s/\\s\\+$//e<CR>', options)

-- Telescope bindings
map('n', '<leader>ff', "<cmd>lua require('telescope.builtin').find_files()<cr>", options)
map('n', '<leader>fg', "<cmd>lua require('telescope.builtin').live_grep()<cr>", options)
