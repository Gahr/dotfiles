local o = vim.o
local wo = vim.wo
local bo = vim.bo

-- Global options.
o.smarttab = true
o.shiftround = true
o.showmatch = true
o.gdefault = true
o.scrolloff = 5
o.visualbell = true
-- o.noerrorbells = true
o.title = true
o.clipboard = 'unnamedplus'
o.laststatus = 3

-- Window local options.
-- wo.nowrap = true
wo.cursorline = true
wo.relativenumber = true
wo.number = true

-- Buffer local options.
bo.tabstop = 8
bo.softtabstop = 0
bo.expandtab = true
bo.shiftwidth = 4
bo.syntax = 'on'
bo.autoindent = true
bo.copyindent = true
bo.filetype = 'on'
