export LANG=en_US.UTF-8

# Add functions path
fpath=($HOME/.funczsh $fpath)
autoload ${fpath[1]}/*(:t)

# Load aliases
if [[ -r ~/.aliasrc ]]; then
    . ~/.aliasrc
fi

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vi'
else
    export EDITOR='nvim'
fi


###########
# Options #
###########

# Turn off beepiong
setopt NO_BEEP

# cd without cd
setopt AUTO_CD

# vim-mode
bindkey -v
# Faster vim-mode
export KEYTIMEOUT=1
# Fix backspace after normal mode
bindkey "^?" backward-delete-char


########
# Path #
########

# Add Android stuff to path
export PATH="/Users/wesleygahr/Library/Android/sdk/platform-tools:$PATH"
export PATH="/Users/wesleygahr/Library/Android/sdk/build-tools/25.0.2:$PATH"
export PATH="/Users/wesleygahr/Library/Android/sdk/ndk-bundle:$PATH"

# Use GNU coreutils
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

# Expose your own binaries
export PATH="/usr/local/bin:$PATH"

# Add Go to path
export PATH="/Users/wesleygahr/go/bin:$PATH"

# Use Brew's OpenSSL
export OPENSSL_LIB_DIR="/usr/local/opt/openssl/lib"
export OPENSSL_INCLUDE_DIR="/usr/local/opt/openssl/include"

# Rust source path
export RUST_SRC_PATH=/Users/wesleygahr/.rustup/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src/rust/src
# Add Rust stuff to path
source $HOME/.cargo/env

# Terminal config
export TERM=xterm-256color
export NVIM_TUI_ENABLE_TRUE_COLOR=1

# Add syntax highlighting to shell
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# FZF config
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!{.git,target,venv}/*" --type-add "blacklist:*.{class,pyc}" -Tblacklist'

# Initialize fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Initialize starship
eval "$(starship init zsh)"
