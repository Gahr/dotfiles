function frida-server --argument-names 'action'
    if test -z "$action"
        echo No action given. 1>&2
        return
    end


    switch $action
        case start
            adb wait-for-usb-device
            timeout 1.0s adb shell su -c /data/local/tmp/frida-server && sleep 2147483647 &
        case stop
            adb wait-for-usb-device
            adb shell su -c pkill frida-server
        case restart
            frida-server stop
            frida-server start
        case '*'
            echo Invalid option 1>&2
    end
end
