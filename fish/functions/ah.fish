function ah --argument-names 'action' 'second'
    if test -z "$action"
        echo No action given. 1>&2
        return
    end

    switch $action
        case l
            if test -z "$second"
                echo No pattern provided. 1>&2
                return
            end

            adb wait-for-usb-device
            adb shell pm list packages -f | grep -i $second
        case dla
            if test -z "$second"
                echo No apk name provided. 1>&2
                return
            end

            adb wait-for-usb-device
            set path (adb shell pm list packages -f $second | cut -d ":" -f 2 | cut -d "=" -f 1)
            adb pull "$path=="
    end
end



