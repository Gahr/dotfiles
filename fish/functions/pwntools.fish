function pwntools
    docker image ls | grep "^pwntools" > /dev/null

    if test $status -ne 0
        echo "The pwntools image is unavailable"
        return 1
    end

    docker run --privileged --interactive --tty --workdir=/root --volume ~/guest:/root/host pwntools
end

