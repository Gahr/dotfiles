function atmel-studio
    if count $argv > /dev/null
        set cal current_dir (pwd)
        set bin "$current_dir/main.bin"
        set hex "$current_dir/main.hex"
        set file $argv[1]
        set ip $argv[2]

        avr-gcc -Wall -g -Os -mmcu=atmega328p -o $bin $file
        avr-objcopy -j .text -j .data -O ihex $bin $hex

        scp $hex uploader@$ip:~/upload/main.hex
        ssh -t uploader@$ip 'cd upload; sudo make flash;'

        rm -f $hex $bin
    else
        echo "Not enough arguments passed"
    end
end

