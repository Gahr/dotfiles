#!/usr/bin/fish

source $HOME/.cargo/env


# Vim keybindings
fish_vi_key_bindings

set --global fish_user_paths "/usr/local/sbin" $fish_user_paths

function my_hist --on-event fish_preexec --description "Track fish history in file"
    echo $argv >> ~/.fish_history
end

set PATH /usr/local/opt/coreutils/libexec/gnubin $PATH

###############
# Environment #
###############

set --export LC_ALL en_US.UTF-8
set --export LANG en_US.UTF-8
set --export FZF_DEFAULT_COMMAND 'rg --files --no-ignore --hidden --follow --glob "!{.git,target,venv}/*" --type-add "blacklist:*.{class,pyc}" -Tblacklist'
set --export EDITOR nvim
set --export ZETTELKASTEN_DIR "/mnt/hdd0/zettelkasten"

#################
# Abbreviations #
#################

if type -q exa
    abbr --add --global e exa
    abbr --add --global ea exa --all
else
    abbr --add --global e ls
    abbr --add --global ea ls --all
end

abbr --add --global cdc cd ~/Code
abbr --add --global cdu cd ..

abbr --add --global g git
abbr --add --global sdn sudo shutdown -h now
abbr --add --global sr sudo reboot
abbr --add --global v $EDITOR
abbr --add --global ash adb shell
abbr --add --global t tmux

abbr --add --global ops "eval (op signin voider1)"

switch (uname)
    case Linux
        abbr --add --global sdn sudo shutdown -h now
        abbr --add --global update "sudo apt update && sudo apt upgrade"

        for drive in (ls /mnt)
            abbr --add --global "$drive" "cd /mnt/$drive"
        end
    case Darwin
        abbr --add --global sdn sudo shutdown
        abbr --add --global cdp cd ~/Pentests
        abbr --add --global update "brew update && brew upgrade && brew cask upgrade"
end




##########
# Prompt #
##########

starship init fish | source
